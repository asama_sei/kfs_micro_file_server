require 'socket'                # 获取socket标准库
require 'gtk3'

class ServerRequire
  def initialize(host)
    @host = host
    @httphead = Array.new
    getinfo = host.gets
    @getType = getinfo.split(' ')[0]
    @getPath = getinfo.split(' ')[1]
    getinfo = host.gets
    while getinfo != "\r\n"
      @httphead << getinfo
      getinfo = host.gets
    end 
    @httphead.each do |headline|
      @hostPath = headline.split(':')[1].strip+':'+headline.split(':')[2].chomp if headline =~ /^Host:/
    end
    @thisWd = Dir.getwd
    p @hostPath+@getPath
  end
  def safeChk
    @unsafe = false
    parts = @getPath.split('/')
    parts.delete_at 0
    parts.each do |part|
      @unsafe = true if part.empty? || part == '.' || part == '..'
    end
  end
  def fileType
    @haveFile = 0 # ->  0:cant find file  1:have file  2:dir
    @haveFile = 1 if File.exist? @thisWd+@getPath
    @haveFile = 2 if File.directory? @thisWd+@getPath

    @fileTyp = 'text/html'
    @fileTyp = 'application/octet-stream' if @haveFile == 1
  end
  def dirList
    #make dir links
    return false if !Dir.exist? @thisWd+@getPath
    @dirPath = Dir.new @thisWd+@getPath
    @dirPath.each do |filename|
      next if filename == '.' || filename == '..'
      @page << "IS A DIR " if Dir.exist? @thisWd+@getPath+'/'+filename
      @page << "<a href = \"#{'http://'+@hostPath + @getPath + (@getPath[-1]=='/'?'':'/') + filename}\">#{filename}</a>"
      @page << "<br>"
    end 

    # make link of 'UP'
    upPath = String.new
    upPath << "http://" + @hostPath
    parts = @getPath.split('/')
    parts.delete_at 0 if parts[-1]!=nil
    parts.delete_at(-1) if parts[-1]!=nil
    parts.each do |filename|
      upPath << '/' + filename
    end
    @page << "<br><a href =\"#{upPath}\">UP</a><br>"
  end
  def make_return_head
    @pagehead << "HTTP/1.0 200 OK\r\n"
  end
  def make_unsafe
    @pagehead << "Content-Type:text/plain\r\nContent-Length: 7\r\nConnection: close\r\n\r\nUNSAFE\n"
    @host.pring @pagehead
    @host.print @page
  end
  def make_file
    File.open(@thisWd+@getPath,"rb") do |file|
      @pagehead << "Content-Type: #{@fileTyp}\r\nContent-Length: #{file.size}\r\nConnection: close\r\n\r\n"
      @host.print @pagehead
      @host.print @page
      IO.copy_stream file,@host
    end
  end
  def make
    @page = String.new
    @pagehead = String.new
    self.safeChk
    self.fileType
    self.make_return_head
    self.make_unsafe if @unsafe
    self.make_file if @haveFile == 1
    if @haveFile == 2
      @pagehead << "Content-Type: text/html\r\nConnection: close\r\n\r\n" 
      @page << "<html><p>"
      @page << "Wrong" if !self.dirList
      @page << "</p></html>"
      @host.print @pagehead
      @host.print @page
    end
  end
end
class RunServer
  def initialize
    @server = TCPServer.open 2000
    @thr = Thread.new{}
    @serthr = Thread.new{}
    @stop = 1
    @links = 0
  end
  def links
    @links
  end
  def run(lab)
    @thr = Thread.new do
      @stop = 0
      loop {                          # 永久运行服务
        break if @stop == 1
        @serthr = Thread.start(@server.accept) do |client|
          @links = @links + 1
          Thread.start do lab.set_markup @links.to_string end
          page = ServerRequire.new client
          page.make
          client.close                # 关闭客户端连接
          @links = @links - 1
          Thread.start do lab.set_markup @links.to_string end
        end
      }
    end
  end
  def stop?
    @stop == 1
  end
  def stop
    @thr.exit
    @serthr.exit
    @stop = 1
  end
end

window = Gtk::Builder.new
server = RunServer.new
window.add 'form.glade'

window['ver2'].set_markup "Ver 0.0.2"
window['welcome'].signal_connect("delete_event") do
  server.stop if !server.stop?
  Gtk.main_quit
end
window['start'].signal_connect("clicked") do
  server.run window['conn'] if server.stop?
end
window['stop'].signal_connect("clicked") do
  server.stop if !server.stop?
end


window['welcome'].show_all

Gtk.main
